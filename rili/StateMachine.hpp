#pragma once
/** @file */

#include <type_traits>
#include <utility>

namespace rili {
/**
 * @brief The StateMachine class is basic implementation of object oriented, polimorphic Finite State Machine. It is
 * quiet similar to State design pattern.
 *
 * This implementation assume, that in every state any possible input event should be somehow handled (even by thrown
 * exception), so to define all possible input events and "default" handlers for them StateBaseType is used.
 *
 * Other states must derive from it and define custom behaviours of handlers. In one moment in state machine is only one
 * state. So to define whole behaviours of given state polimorphism is strongly used. From usage point of view state is
 * just set of handlers for some events with inheritance chain for handling all other.
 *
 * Example State:
 * @code
 * struct State: public SomeOther
 * {
 *     virtual ~State() = default;
 *     virtual BaseState const& handle(int const&, StorageType&) const override;
 *     virtual BaseState const& handle(double const&, StorageType&) const override;
 *     virtual BaseState const& handle(std::string const&, StorageType&) const override;
 *
 *     static inline State const& instance(){
 *         static State instance;
 *         return instance;
 *     }
 *  private:
 *     State();
 * }
 * @endcode
 *
 * @note All 'handle' members in state should be const - non const will not be choosen. This is to force correct usage
 * of State.
 */
template <typename StateBaseType, typename StorageType = std::nullptr_t>
class StateMachine {
    static_assert(std::has_virtual_destructor<StateBaseType>::value, "BaseState must have virtual destructor");
    static_assert(std::is_nothrow_default_constructible<StateBaseType>::value,
                  "BaseState must be no throw default constructiable");

 public:
    /**
     * @brief initialize instance of StateMachine using StateBaseType instance(or inherited) and StorageType storage
     * @param initState is used as first state to handle events
     * @param storage is used to initialize internal StateMachine storage (is copied)
     */
    explicit StateMachine(StateBaseType const& initState, StorageType const& storage = StorageType()) noexcept
        : m_state(initState),
          m_storage(storage) {}

    /**
     * @brief is used as sink for all events which should be handled by StateMachine.
     *
     * It will choose and call one handler in current State, which can handle given event.
     * After handling, it will set state to that, returned by handle method of state for particular event.
     *
     * @param event
     */
    template <typename EventType>
    void handle(EventType const& event) {
        m_state = std::reference_wrapper<const StateBaseType>(m_state.get().handle(event, m_storage));
    }

    /**
     * @brief give access to StateMachine storage
     * @return storage object reference
     */
    StorageType& storage() noexcept { return m_storage; }

    /**
     * @brief give access to StateMachine storage
     * @return storage object reference
     */
    StorageType const& storage() const noexcept { return m_storage; }

 private:
    std::reference_wrapper<const StateBaseType> m_state;
    StorageType m_storage;
};
}  // namespace rili
