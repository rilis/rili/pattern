#pragma once
/** @file */

#include <rili/Variant.hpp>
#include <type_traits>

namespace rili {
namespace detail {
namespace variant {
template <typename...>
struct IsBaseOf {
    static constexpr bool value = false;
};

template <typename T, typename S>
struct IsBaseOf<T, S> {
    static constexpr bool value = std::is_base_of<T, S>::value || std::is_same<T, S>::value;
};

template <typename T, typename S, typename... Types>
struct IsBaseOf<T, S, Types...> {
    static constexpr bool value =
        (std::is_base_of<T, S>::value || std::is_same<T, S>::value) && IsBaseOf<T, Types...>::value;
};
}  // namespace variant
}  // namespace detail

/**
 * @brief SiblingVariant is data structure which can be used like union of given Base type derived types
 */
template <typename Base, typename... Types>
class SiblingVariant : public Variant<Types...> {
    static_assert(detail::variant::IsBaseOf<Base, Types...>::value,
                  "Provided Base parameter type is not base of all child types");

 public:
    /**
     * @brief base used to get reference to base class of all possibly holded types
     * @return base class reference
     */
    template <typename T>
    T& base() noexcept {
        static_assert(detail::variant::IsBaseOf<T, Types...>::value, "Provided type is not base of all child types");
        return *reinterpret_cast<T*>(&this->m_data);
    }

    /**
     * @brief base used to get reference to base class of all possibly holded types
     * @return base class reference
     */
    template <typename T>
    T const& base() const noexcept {
        static_assert(detail::variant::IsBaseOf<T, Types...>::value, "Provided type is not base of all child types");
        return *reinterpret_cast<T const*>(&this->m_data);
    }

    /**
     * @brief used to get value of Variant
     *
     * @warning if type T is not equal to currently stored type in Variant or not base of allpossible types
     * std::bad_cast will be thrown
     * @return current Variant value
     */
    template <typename T>
    T& get() {
        static_assert(detail::variant::IsBaseOf<T, Types...>::value || detail::variant::IsOneOf<T, Types...>::value,
                      "Provided type is not base of all child types nor any of holded child types");
        if (!detail::variant::IsBaseOf<T, Types...>::value && this->m_id != TypeId::create<T>()) {
            throw std::bad_cast();
        }
        return *reinterpret_cast<T*>(&this->m_data);
    }

    /**
     * @brief used to get value of Variant
     *
     * @warning if type T is not equal to currently stored type in Variant or not base of allpossible types
     * std::bad_cast will be thrown
     * @return current Variant value
     */
    template <typename T>
    T const& get() const {
        static_assert(detail::variant::IsBaseOf<T, Types...>::value || detail::variant::IsOneOf<T, Types...>::value,
                      "Provided type is not base of all child types nor any of holded child types");
        if (!detail::variant::IsBaseOf<T, Types...>::value && this->m_id != TypeId::create<T>()) {
            throw std::bad_cast();
        }
        return *reinterpret_cast<T const*>(&this->m_data);
    }
};
}  // namespace rili
