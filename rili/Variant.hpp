#pragma once
/** @file */

#include <algorithm>
#include <rili/TypeId.hpp>
#include <string>
#include <type_traits>
#include <typeinfo>
#include <utility>

namespace rili {
namespace detail {
namespace variant {
template <typename... Ts>
struct MaxTypeInfo;

template <typename T1, typename... Ts>
struct MaxTypeInfo<T1, Ts...> {
    static constexpr std::size_t align = alignof(T1) > MaxTypeInfo<Ts...>::align ? alignof(T1)
                                                                                 : MaxTypeInfo<Ts...>::align;
    static constexpr std::size_t size = sizeof(T1) > MaxTypeInfo<Ts...>::size ? sizeof(T1) : MaxTypeInfo<Ts...>::size;
};

template <typename T1>
struct MaxTypeInfo<T1> {
    static constexpr std::size_t align = alignof(T1);
    static constexpr std::size_t size = sizeof(T1);
};

template <class... Types>
struct aligned_union {
    struct type {
        alignas(MaxTypeInfo<Types...>::align) char _s[MaxTypeInfo<Types...>::size];
    };
};

template <typename...>
struct IsOneOf {
    static constexpr bool value = false;
};

template <typename T, typename S, typename... Types>
struct IsOneOf<T, S, Types...> {
    static constexpr bool value = std::is_same<T, S>::value || IsOneOf<T, Types...>::value;
};

template <typename T>
void destroyHelper(void* ptr) {
    reinterpret_cast<T*>(ptr)->~T();
}

template <typename T>
void copyHelper(void* self, void const* other) {
    new (self) T(*reinterpret_cast<const T*>(other));
}

template <typename T>
void moveHelper(void* self, void* other) {
    new (self) T(std::move(*reinterpret_cast<T*>(other)));
}
}  // namespace variant
}  // namespace detail

/**
 * @brief Variant is data structure which can be used like union but allow to store non-POD types
 */
template <typename... Types>
struct Variant {
 private:
    typedef typename detail::variant::aligned_union<Types...>::type Data;

 public:
    /**
     * @brief Variant initialize Variant with invalid value
     */
    Variant() : m_copy(nullptr), m_move(nullptr), m_destroy(nullptr), m_id(TypeId()) {}

    /**
     * @brief Variant copy constructor
     * @param other other Variant
     */
    Variant(const Variant<Types...>& other) : Variant() {
        if (other.m_copy) {
            other.m_copy(&m_data, &other.m_data);
        }

        m_destroy = other.m_destroy;
        m_copy = other.m_copy;
        m_move = other.m_move;
        m_id = other.m_id;
    }

    /**
     * @brief Variant move constructor
     * @param other other Variant
     */
    Variant(Variant<Types...>&& other) : Variant() {
        if (other.m_move) {
            other.m_move(&m_data, &other.m_data);
        }

        m_destroy = other.m_destroy;
        m_copy = other.m_copy;
        m_move = other.m_move;
        m_id = other.m_id;

        other.clearDetails();
    }

    /**
     * @brief operator = is Variant move assignment operator
     * @param other other Variant
     * @return
     */
    Variant<Types...>& operator=(Variant<Types...>&& other) {
        if (this != &other) {
            if (m_destroy) {
                m_destroy(&m_data);
            }

            clearDetails();
            if (other.m_move) {
                other.m_move(&m_data, &other.m_data);
            }

            m_destroy = other.m_destroy;
            m_copy = other.m_copy;
            m_move = other.m_move;
            m_id = other.m_id;

            other.clearDetails();
        }
        return *this;
    }

    /**
     * @brief operator = is Variant copy assignment operator
     * @param other other Variant
     * @return
     */
    Variant<Types...>& operator=(Variant<Types...> const& other) {
        if (this != &other) {
            if (m_destroy) {
                m_destroy(&m_data);
            }

            clearDetails();
            if (other.m_copy) {
                other.m_copy(&m_data, &other.m_data);
            }

            m_destroy = other.m_destroy;
            m_copy = other.m_copy;
            m_move = other.m_move;
            m_id = other.m_id;
        }
        return *this;
    }

    /**
     * @brief Check if currently stored value in Variant is given T type.
     * @return true is types match, false otherwise
     */
    template <typename T>
    bool is() const {
        static_assert(detail::variant::IsOneOf<T, Types...>::value, "Having this type in this Variant is not possible");
        return m_id == TypeId::create<T>();
    }

    /**
     * @brief valid check if Variant is correctly initialized
     * @return true is correctly initialized, false otherwise
     */
    bool valid() { return m_id != TypeId(); }

    /**
     * @brief used to set value of Variant and change type if needed
     * @param args value to be set
     */
    template <typename T, typename... Args>
    void set(Args&&... args) {
        static_assert(detail::variant::IsOneOf<T, Types...>::value,
                      "You can not set value of given type to this Variant");
        if (m_destroy) {
            m_destroy(&m_data);
        }
        clearDetails();
        new (&m_data) T(std::forward<Args>(args)...);
        setDetails<T>();
    }

    /**
     * @brief used to get value of Variant
     *
     * @warning if type T is not equal to currently stored type in Variant std::bad_cast will be thrown
     * @return current Variant value
     */
    template <typename T>
    T& get() {
        static_assert(detail::variant::IsOneOf<T, Types...>::value,
                      "You can not gat value of given type from this Variant");
        if (m_id != TypeId::create<T>()) {
            throw std::bad_cast();
        }
        return *reinterpret_cast<T*>(&m_data);
    }

    /**
     * @brief used to get value of Variant
     * @return current Variant value
     *
     * @warning if type T is not equal to currently stored type in Variant std::bad_cast will be thrown
     */
    template <typename T>
    T const& get() const {
        static_assert(detail::variant::IsOneOf<T, Types...>::value,
                      "You can not gat value of given type from this Variant");
        if (m_id != TypeId::create<T>()) {
            throw std::bad_cast();
        }
        return *reinterpret_cast<T const*>(&m_data);
    }

    ~Variant() {
        if (m_destroy) {
            m_destroy(&m_data);
        }
    }
    /// @cond INTERNAL
 protected:
    void clearDetails() noexcept {
        m_id = TypeId();
        m_copy = nullptr;
        m_move = nullptr;
        m_destroy = nullptr;
    }

    template <typename T>
    void setDetails() noexcept {
        m_id = TypeId::create<T>();
        m_copy = detail::variant::copyHelper<T>;
        m_move = detail::variant::moveHelper<T>;
        m_destroy = detail::variant::destroyHelper<T>;
    }

 protected:
    void (*m_copy)(void* self, void const* other);
    void (*m_move)(void* self, void* other);
    void (*m_destroy)(void* self);
    TypeId m_id;
    Data m_data;
    /// @endcond INTERNAL
};
}  // namespace rili
