#include <chrono>
#include <rili/StateMachine.hpp>
#include <rili/Test.hpp>

class PersistantData {
 public:
    PersistantData() : timeInRed(0), timeInGreen(0), timeInYellow(0), timeInOutOfOrder(0) {}
    int timeInRed;
    int timeInGreen;
    int timeInYellow;
    int timeInOutOfOrder;
};

struct Timeout {};
struct Change {};
struct Break {};
struct Fix {};
struct Throw {};

class BaseState {
 public:
    virtual ~BaseState() = default;
    virtual BaseState const &handle(Timeout const &, PersistantData &) const;
    virtual BaseState const &handle(Change const &, PersistantData &) const;
    virtual BaseState const &handle(Break const &, PersistantData &) const;
    virtual BaseState const &handle(Fix const &, PersistantData &) const;
    virtual BaseState const &handle(Throw const &, PersistantData &) const;
};

typedef rili::StateMachine<BaseState, PersistantData> TestStateMachine;

class Red : public BaseState {
 protected:
    Red() noexcept : BaseState() {}

 public:
    static Red const &get() noexcept {
        static const Red instance;
        return instance;
    }
    BaseState const &handle(Timeout const &, PersistantData &) const override;
    BaseState const &handle(Change const &, PersistantData &) const override;
    virtual ~Red() = default;
};
class Green : public BaseState {
 protected:
    Green() noexcept : BaseState() {}

 public:
    static Green const &get() noexcept {
        static const Green instance;
        return instance;
    }
    BaseState const &handle(Timeout const &, PersistantData &) const override;
    BaseState const &handle(Change const &, PersistantData &) const override;
    virtual ~Green() = default;
};
class Yellow : public BaseState {
 protected:
    Yellow() noexcept : BaseState() {}

 public:
    static Yellow const &get() noexcept {
        static const Yellow instance;
        return instance;
    }
    BaseState const &handle(Timeout const &, PersistantData &) const override;
    BaseState const &handle(Change const &, PersistantData &) const override;
    virtual ~Yellow() = default;
};
class OutOfOrder : public BaseState {
 protected:
    OutOfOrder() noexcept : BaseState() {}

 public:
    static OutOfOrder const &get() noexcept {
        static const OutOfOrder instance;
        return instance;
    }
    BaseState const &handle(Timeout const &, PersistantData &) const override;
    BaseState const &handle(Change const &, PersistantData &) const override;
    BaseState const &handle(Break const &, PersistantData &) const override;
    BaseState const &handle(Fix const &, PersistantData &) const override;
    virtual ~OutOfOrder() = default;
};

BaseState const &BaseState::handle(const Timeout &, PersistantData &) const { return OutOfOrder::get(); }

BaseState const &BaseState::handle(const Change &, PersistantData &) const { return OutOfOrder::get(); }
BaseState const &BaseState::handle(const Break &, PersistantData &) const { return OutOfOrder::get(); }
BaseState const &BaseState::handle(const Fix &, PersistantData &) const { return OutOfOrder::get(); }

BaseState const &BaseState::handle(const Throw &, PersistantData &) const { throw 5; }

BaseState const &Red::handle(const Change &, PersistantData &) const { return Green::get(); }
BaseState const &Green::handle(const Change &, PersistantData &) const { return Yellow::get(); }
BaseState const &Yellow::handle(const Change &, PersistantData &) const { return Red::get(); }

BaseState const &Red::handle(const Timeout &, PersistantData &data) const {
    data.timeInRed++;
    return *this;
}
BaseState const &Green::handle(const Timeout &, PersistantData &data) const {
    data.timeInGreen++;
    return *this;
}
BaseState const &Yellow::handle(const Timeout &, PersistantData &data) const {
    data.timeInYellow++;
    return *this;
}

BaseState const &OutOfOrder::handle(const Fix &, PersistantData &) const { return Red::get(); }
BaseState const &OutOfOrder::handle(const Change &, PersistantData &) const { return *this; }
BaseState const &OutOfOrder::handle(const Timeout &, PersistantData &data) const {
    data.timeInOutOfOrder++;
    return *this;
}
BaseState const &OutOfOrder::handle(const Break &, PersistantData &) const { return *this; }

TEST(StateMachine, WhenNoEventsHandled) {
    TestStateMachine sm(Red::get());
    EXPECT_EQ(0, sm.storage().timeInGreen);
    EXPECT_EQ(0, sm.storage().timeInYellow);
    EXPECT_EQ(0, sm.storage().timeInRed);
    EXPECT_EQ(0, sm.storage().timeInOutOfOrder);
}

TEST(StateMachine, WhenSingleCycleWithoutAnyTimeout) {
    TestStateMachine sm(Red::get());

    sm.handle(Change{});
    sm.handle(Timeout{});
    sm.handle(Change{});
    sm.handle(Timeout{});
    sm.handle(Change{});
    sm.handle(Timeout{});

    EXPECT_EQ(1, sm.storage().timeInGreen);
    EXPECT_EQ(1, sm.storage().timeInYellow);
    EXPECT_EQ(1, sm.storage().timeInRed);
    EXPECT_EQ(0, sm.storage().timeInOutOfOrder);
}

TEST(StateMachine, WhenSingleBreakAndNextCycle) {
    TestStateMachine sm(Red::get());

    sm.handle(Change{});
    sm.handle(Timeout{});
    sm.handle(Change{});
    sm.handle(Timeout{});
    sm.handle(Change{});
    sm.handle(Timeout{});

    sm.handle(Break{});
    sm.handle(Timeout{});
    sm.handle(Change{});
    sm.handle(Timeout{});
    sm.handle(Change{});
    sm.handle(Timeout{});
    sm.handle(Break{});
    sm.handle(Timeout{});
    sm.handle(Change{});
    sm.handle(Timeout{});
    sm.handle(Change{});
    sm.handle(Timeout{});
    sm.handle(Fix{});

    sm.handle(Timeout{});
    sm.handle(Change{});
    sm.handle(Timeout{});
    sm.handle(Change{});
    sm.handle(Timeout{});

    EXPECT_EQ(2, sm.storage().timeInGreen);
    EXPECT_EQ(2, sm.storage().timeInYellow);
    EXPECT_EQ(2, sm.storage().timeInRed);
    EXPECT_EQ(6, sm.storage().timeInOutOfOrder);
}

TEST(StateMachine, WhenThrowEventThenThrows) {
    TestStateMachine sm(Red::get());
    try {
        sm.handle(Throw{});
    } catch (int e) {
        EXPECT_EQ(5, e);
    } catch (...) {
        ADD_FAILURE("Wrong exception type");
    }
}
