#include <memory>
#include <rili/SiblingVariant.hpp>
#include <rili/Test.hpp>
#include <rili/Variant.hpp>
#include <utility>

TEST(Variant, WhenConstructedThenUninitialized) {
    rili::Variant<bool, int, std::shared_ptr<int>> variant;

    EXPECT_FALSE(variant.valid());
    EXPECT_FALSE(variant.is<bool>());
    EXPECT_FALSE(variant.is<int>());
    EXPECT_FALSE(variant.is<std::shared_ptr<int>>());

    EXPECT_THROW(std::bad_cast, [&variant]() { variant.get<bool>(); });
    EXPECT_THROW(std::bad_cast, [&variant]() { variant.get<int>(); });
    EXPECT_THROW(std::bad_cast, [&variant]() { variant.get<std::shared_ptr<int>>(); });
}

TEST(Variant, WhenSetThenValidAndAccesiable) {
    rili::Variant<bool, int, std::shared_ptr<int>> variant;

    variant.set<bool>(true);

    EXPECT_TRUE(variant.valid());
    EXPECT_TRUE(variant.is<bool>());
    EXPECT_FALSE(variant.is<int>());
    EXPECT_FALSE(variant.is<std::shared_ptr<int>>());

    variant.get<bool>();
    EXPECT_THROW(std::bad_cast, [&variant]() { variant.get<int>(); });
    EXPECT_THROW(std::bad_cast, [&variant]() { variant.get<std::shared_ptr<int>>(); });
}

TEST(Variant, WhenSetThenValuePreservedToNextSet) {
    rili::Variant<bool, int, std::shared_ptr<int>> variant;
    std::weak_ptr<int> w;
    {
        auto s = std::make_shared<int>(5);
        variant.set<std::shared_ptr<int>>(s);
        w = s;
    }
    EXPECT_EQ(w.use_count(), 1);
    {
        auto s = variant.get<std::shared_ptr<int>>();
        EXPECT_EQ(*s, 5);
    }
    EXPECT_EQ(w.use_count(), 1);
    {
        variant.set<std::shared_ptr<int>>(std::make_shared<int>(10));  // NOLINT
    }
    EXPECT_EQ(w.use_count(), 0);
}
namespace {
struct A {
    explicit A(int i) : m_i(i) {}
    int m_i;
};
struct B : public A {};
struct C : public A {
    explicit C(int i) : A(i) {}
};
struct D : public C {
    explicit D(int i) : C(i) {}
};
}  // namespace

TEST(SiblingVariant, CanBeSetAndAccesed) {
    rili::SiblingVariant<A, B, C, D> v;

    EXPECT_FALSE(v.valid());
    v.set<D>(D(5));  // NOLINT (build/include_what_you_use)
    EXPECT_TRUE(v.valid());
    EXPECT_TRUE(v.is<D>());
    EXPECT_EQ(v.base<A>().m_i, 5);
    EXPECT_EQ(v.get<A>().m_i, 5);
}

TEST(SiblingVariant, CanBeCopiedUsingCAOperator) {
    rili::SiblingVariant<A, B, C, D> v;
    v.set<D>(D(5));  // NOLINT (build/include_what_you_use)

    rili::SiblingVariant<A, B, C, D> other;
    other.set<C>(C(10));  // NOLINT (build/include_what_you_use)

    other = v;

    EXPECT_EQ(other.get<D>().m_i, 5);
    other.get<D>().m_i = 6;
    EXPECT_EQ(v.get<D>().m_i, 5);
}

TEST(SiblingVariant, CanBeCopiedUsingCCtor) {
    rili::SiblingVariant<A, B, C, D> v;
    v.set<D>(D(5));  // NOLINT (build/include_what_you_use)

    rili::SiblingVariant<A, B, C, D> other(v);

    EXPECT_EQ(other.get<D>().m_i, 5);
    other.get<D>().m_i = 6;
    EXPECT_EQ(v.get<D>().m_i, 5);
}

TEST(SiblingVariant, CanBeMovedUsingMCtor) {
    rili::SiblingVariant<A, B, C, D> v;
    v.set<D>(D(5));  // NOLINT (build/include_what_you_use)

    rili::SiblingVariant<A, B, C, D> other(std::move(v));

    EXPECT_EQ(other.get<D>().m_i, 5);
    EXPECT_FALSE(v.valid());
}

TEST(SiblingVariant, CanBeMovedUsingMAoperator) {
    rili::SiblingVariant<A, B, C, D> v;
    v.set<D>(D(5));  // NOLINT (build/include_what_you_use)

    rili::SiblingVariant<A, B, C, D> other;
    other = std::move(v);

    EXPECT_EQ(other.get<D>().m_i, 5);
    EXPECT_FALSE(v.valid());
}
