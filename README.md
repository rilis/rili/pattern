# Rili Pattern

This is module of [rili](https://gitlab.com/rilis/rili) which provide some usefull patterns:
 * StateMachine
 * Variant
 * TypeId


**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/pattern)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/pattern/badges/master/build.svg)](https://gitlab.com/rilis/rili/pattern/commits/master)
[![coverage report](https://gitlab.com/rilis/rili/pattern/badges/master/coverage.svg)](https://gitlab.com/rilis/rili/pattern/commits/master)
